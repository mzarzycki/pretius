import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BigDecimal totalAmount = new BigDecimal("0");

		try {
			ArrayList<String> transactions = new ArrayList<String>();

			File file;
			if (args.length == 0) {
				file = new File("Plik z danymi.txt");
			} else if (args.length == 1) {
				file = new File(args[0]);
			} else {
				System.err.println("wrong params");
				file = new File("Plik z danymi.txt");
			}

			Scanner sc = new Scanner(file, "utf-8");
			while (sc.hasNextLine()) {
				transactions.add(sc.nextLine());
			}
			sc.close();

			for (String transaction : transactions) {
				if (transaction.isEmpty() || transaction == null || transaction == "") {
					continue;
				}
				String[] splits = transaction.split("@name:|@src_iban:|@dst_iban:|@amount:");
				String amountCurrency = splits[4];
				String amount = amountCurrency.replaceAll("[^0-9?!\\,]", "");
				totalAmount = totalAmount.add(new BigDecimal(amount.replaceAll(",", ".")));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println(totalAmount);

	}
}
